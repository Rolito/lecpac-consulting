<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;

/**
 * Customer
 *
 * @ORM\Table(name="customer")
 * @ORM\Entity
 * @ApiResource
 */
class Customer
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="raison_sociale", type="string", length=255, nullable=true)
     */
    private $raisonSociale;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nom_contact", type="string", length=255, nullable=true)
     */
    private $nomContact;

    /**
     * @var string|null
     *
     * @ORM\Column(name="prenom_contact", type="string", length=255, nullable=true)
     */
    private $prenomContact;

    /**
     * @var string|null
     *
     * @ORM\Column(name="civilite", type="string", length=255, nullable=true)
     */
    private $civilite;

    /**
     * @var string|null
     *
     * @ORM\Column(name="fonction", type="string", length=255, nullable=true)
     */
    private $fonction;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tel_contact", type="string", length=255, nullable=true)
     */
    private $telContact;

    /**
     * @var string|null
     *
     * @ORM\Column(name="portable_contact", type="string", length=255, nullable=true)
     */
    private $portableContact;

    /**
     * @var string|null
     *
     * @ORM\Column(name="email_contact", type="string", length=255, nullable=true)
     */
    private $emailContact;

    /**
     * @var string|null
     *
     * @ORM\Column(name="adresse_1", type="string", length=255, nullable=true)
     */
    private $adresse1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="adresse_2", type="string", length=255, nullable=true)
     */
    private $adresse2;

    /**
     * @var string|null
     *
     * @ORM\Column(name="code_postale_entreprise", type="string", length=255, nullable=true)
     */
    private $codePostaleEntreprise;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ville_entreprise", type="string", length=255, nullable=true)
     */
    private $villeEntreprise;

    /**
     * @var string|null
     *
     * @ORM\Column(name="telephone_entreprise", type="string", length=255, nullable=true)
     */
    private $telephoneEntreprise;

    /**
     * @var int|null
     *
     * @ORM\Column(name="effectif", type="integer", nullable=true)
     */
    private $effectif;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRaisonSociale(): ?string
    {
        return $this->raisonSociale;
    }

    public function setRaisonSociale(?string $raisonSociale): self
    {
        $this->raisonSociale = $raisonSociale;

        return $this;
    }

    public function getNomContact(): ?string
    {
        return $this->nomContact;
    }

    public function setNomContact(?string $nomContact): self
    {
        $this->nomContact = $nomContact;

        return $this;
    }

    public function getPrenomContact(): ?string
    {
        return $this->prenomContact;
    }

    public function setPrenomContact(?string $prenomContact): self
    {
        $this->prenomContact = $prenomContact;

        return $this;
    }

    public function getCivilite(): ?string
    {
        return $this->civilite;
    }

    public function setCivilite(?string $civilite): self
    {
        $this->civilite = $civilite;

        return $this;
    }

    public function getFonction(): ?string
    {
        return $this->fonction;
    }

    public function setFonction(?string $fonction): self
    {
        $this->fonction = $fonction;

        return $this;
    }

    public function getTelContact(): ?string
    {
        return $this->telContact;
    }

    public function setTelContact(?string $telContact): self
    {
        $this->telContact = $telContact;

        return $this;
    }

    public function getPortableContact(): ?string
    {
        return $this->portableContact;
    }

    public function setPortableContact(?string $portableContact): self
    {
        $this->portableContact = $portableContact;

        return $this;
    }

    public function getEmailContact(): ?string
    {
        return $this->emailContact;
    }

    public function setEmailContact(?string $emailContact): self
    {
        $this->emailContact = $emailContact;

        return $this;
    }

    public function getAdresse1(): ?string
    {
        return $this->adresse1;
    }

    public function setAdresse1(?string $adresse1): self
    {
        $this->adresse1 = $adresse1;

        return $this;
    }

    public function getAdresse2(): ?string
    {
        return $this->adresse2;
    }

    public function setAdresse2(?string $adresse2): self
    {
        $this->adresse2 = $adresse2;

        return $this;
    }

    public function getCodePostaleEntreprise(): ?string
    {
        return $this->codePostaleEntreprise;
    }

    public function setCodePostaleEntreprise(?string $codePostaleEntreprise): self
    {
        $this->codePostaleEntreprise = $codePostaleEntreprise;

        return $this;
    }

    public function getVilleEntreprise(): ?string
    {
        return $this->villeEntreprise;
    }

    public function setVilleEntreprise(?string $villeEntreprise): self
    {
        $this->villeEntreprise = $villeEntreprise;

        return $this;
    }

    public function getTelephoneEntreprise(): ?string
    {
        return $this->telephoneEntreprise;
    }

    public function setTelephoneEntreprise(?string $telephoneEntreprise): self
    {
        $this->telephoneEntreprise = $telephoneEntreprise;

        return $this;
    }

    public function getEffectif(): ?int
    {
        return $this->effectif;
    }

    public function setEffectif(?int $effectif): self
    {
        $this->effectif = $effectif;

        return $this;
    }


}
